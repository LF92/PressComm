/*
Łukasz Fałda
Numer indeksu: 115973
Wydział: WIMiI
Kierunek: Infromatyka, 1 stopnia
Specialność: Inżynieria oprogramowania
*/
package space.presscomm;

public class AppConfig {
	public static String URL_LOGIN = "http://pcs.anddev.one/login.php";
	public static String URL_REGISTER = "http://pcs.anddev.one/register.php";
	public static String URL_ALLNEWS = "http://anddev.one/nall.php";
	
    public static final String FILE_UPLOAD_URL = "http://img.anddev.one/index.php";
    public static final String IMAGE_DIRECTORY_NAME = "PressCommImages";
}