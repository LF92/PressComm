/*
Łukasz Fałda
Numer indeksu: 115973
Wydział: WIMiI
Kierunek: Infromatyka, 1 stopnia
Specialność: Inżynieria oprogramowania
*/
package space.presscomm;

import space.presscomm.NewsElement;
import space.presscomm.R;

import java.util.List;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class ListforNews extends BaseAdapter {
	private Typeface robotoLight, robotoRegular;
	private Activity activity;
	private LayoutInflater inflater;
	private List<NewsElement> newsItems;
	
	Context context;
	RequestQueue queue;
	ImageLoader imgLoad;
	
	public ListforNews(Activity activity, List<NewsElement> newsItems) {
		this.activity = activity;
		this.newsItems = newsItems;
	}
	
	@Override public int getCount() {
		return newsItems.size();
	}
	@Override public Object getItem(int location) {
		return newsItems.get(location);
	}
	@Override public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		context = parent.getContext();
		queue = Volley.newRequestQueue(context);
		imgLoad = new ImageLoader(this.queue, new LruBitmapCache());
		if (inflater == null) {
			inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_row, null);
		}
		if (imgLoad == null) {
			imgLoad =  new ImageLoader(this.queue, new LruBitmapCache());
		}

		robotoLight = Typeface.createFromAsset(convertView.getContext().getAssets(), "fonts/RobotoLight.ttf");  
		robotoRegular = Typeface.createFromAsset(convertView.getContext().getAssets(), "fonts/RobotoRegular.ttf"); 
		
		NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.imagenr1);
		
		TextView adress = (TextView) convertView.findViewById(R.id.adressTextView);
		TextView message = (TextView) convertView.findViewById(R.id.descriptionTextView);
		TextView datee = (TextView) convertView.findViewById(R.id.dateeTextView);
		
		NewsElement m = newsItems.get(position);
		
		thumbNail.setImageUrl(m.getImg1(), imgLoad);
		adress.setText(m.getAdress());
		message.setText(m.getMess());
		datee.setText(m.getDatee());
		
		adress.setTypeface(robotoRegular);
		message.setTypeface(robotoRegular);
		datee.setTypeface(robotoLight);
		
		return convertView;
	}
}