/*
Łukasz Fałda
Numer indeksu: 115973
Wydział: WIMiI
Kierunek: Infromatyka, 1 stopnia
Specialność: Inżynieria oprogramowania
*/
package space.presscomm;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import space.presscomm.MainActivity;
import space.presscomm.R;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import space.presscomm.AppConfig;
import space.presscomm.localDB;
import space.presscomm.SessionMgr;

public class LoginActivity extends AppCompatActivity {
	final Context context = LoginActivity.this;
	public static Activity LOGIN_ACTIVITY;
	private Button buttonLOGIN, buttonREGISTER;
	private EditText editTXTEmail, editTXTPassword;
	private ProgressDialog pDialog;
	private SessionMgr session;
	private localDB DB;
	private RequestQueue queue;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		LOGIN_ACTIVITY = this;
		
		queue = Volley.newRequestQueue(this);
		
		editTXTEmail = (EditText) findViewById(R.id.email);
		editTXTPassword = (EditText) findViewById(R.id.password);
		buttonLOGIN = (Button) findViewById(R.id.buttonLOG);
		buttonREGISTER = (Button) findViewById(R.id.buttonREG);
		
		pDialog = new ProgressDialog(this);
		pDialog.setCancelable(false);

		DB = new localDB(context);
		
		session = new SessionMgr(context);
		
		buttonLOGIN.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				String email = editTXTEmail.getText().toString().trim();
				String password = editTXTPassword.getText().toString().trim();
				
				if((!email.isEmpty()) && (!password.isEmpty())) {
					checkLogin(email, password);
				}else {
					Toast.makeText(context,R.string.allMustBeFilled, Toast.LENGTH_LONG).show();
				}
			}
		});

		buttonREGISTER.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent i = new Intent(context,RegisterActivity.class);
				startActivity(i);
				}
			});
		}
	
		private void checkLogin(final String email, final String password) {
			pDialog.setMessage(getString(R.string.waitLogin));
			showDialog();
			
			StringRequest strReq = new StringRequest(Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				hideDialog();
				try {
					JSONObject jObj = new JSONObject(response);
					boolean error = jObj.getBoolean("error");
					if(!error) {
						session.setLogin(true);
						String uid = jObj.getString("uid");
						String xid = jObj.getString("xid");
						JSONObject user = jObj.getJSONObject("user");
						String name = user.getString("name");
						String email = user.getString("email");
						String created_at = user.getString("created_at");

						DB.addUser(name, email, xid, uid, created_at);

						MainActivity.MAIN_ACTIVITY.finish();
						Intent intent = new Intent(context, MainActivity.class);
						startActivity(intent);
						finish();
					}else {
						String errorMsg = jObj.getString("error_msg");
							Toast.makeText(context,errorMsg, Toast.LENGTH_LONG).show();
						}
}					catch (JSONException e) {
					e.printStackTrace();
					Toast.makeText(context, R.string.jsonerror+": "+ e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
				hideDialog();
			}
		}) {
				@Override
				protected Map<String, String> getParams() {
					Map<String, String> params = new HashMap<String, String>();
					params.put("email", email);
					params.put("password", password);
					return params;
				}
			};
			queue.add(strReq);
		}

	private void showDialog() { if (!pDialog.isShowing()) { pDialog.show(); } }
	private void hideDialog() { if (pDialog.isShowing()) { pDialog.dismiss(); } }
}