/*
Łukasz Fałda
Numer indeksu: 115973
Wydział: WIMiI
Kierunek: Infromatyka, 1 stopnia
Specialność: Inżynieria oprogramowania
*/
package space.presscomm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import space.presscomm.R;

@SuppressWarnings("deprecation")
public class MainFragment extends Fragment implements Camera.PictureCallback {
	private FloatingActionButton buttonAddQuickPhoto;
	private View buttonAddPhoto, buttonaddFromGallery, buttonAddVideo, buttonaddVideoFromGallery;
	private TextView AddEventTextView, AddPhotoTextView, AddFromGalleryTextView, AddVideoTextView, AddVideoFromGalleryTextView;
	private Typeface robotoLight;

	private final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private final int CAMERA_REQUEST_CODE_VIDEO = 133;
	
	private static int ACTIONID = 0; // 1-camera, 2-gallery, 3-fabquicpic
	private String IMAGEVIDEOpath = null;
	private Uri fileUri = null;
	
	Uri selectedImage = null;
	
	String HomeText, MyNewsText, HotText, SettingsText, AboutAppText;
	
	ProgressDialog dialog;
	
	private Camera.Parameters parameters = null;
	private Camera camera = null;
	private int cameraID = 0;
	private static boolean isSectedFAB = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.main_fragment,container,false);
		
		checkStorage();
		robotoLight = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RobotoLight.ttf"); 
		
		HomeText = getString(R.string.home);
		MyNewsText = getString(R.string.mynews);
		HotText = getString(R.string.hot);
		SettingsText = getString(R.string.action_settings);
		AboutAppText = getString(R.string.about);
			
		buttonAddPhoto = v.findViewById(R.id.addPhoto);
		buttonAddVideo = v.findViewById(R.id.addVideo);
		buttonaddFromGallery = v.findViewById(R.id.addFromGallery);
		buttonaddVideoFromGallery = v.findViewById(R.id.addVideoFromGallery);
		
		AddEventTextView = (TextView) v.findViewById(R.id.addEvent);
		AddPhotoTextView = (TextView) v.findViewById(R.id.addPhoto_text);
		AddFromGalleryTextView = (TextView) v.findViewById(R.id.addFromGallery_text);
		AddVideoTextView = (TextView) v.findViewById(R.id.addVideo_text);
		AddVideoFromGalleryTextView = (TextView) v.findViewById(R.id.addVideoFromGallery_text);
		
		buttonAddQuickPhoto = (FloatingActionButton) v.findViewById(R.id.fab);
		
		AddEventTextView.setTypeface(robotoLight);
		AddPhotoTextView.setTypeface(robotoLight);
		AddFromGalleryTextView.setTypeface(robotoLight);
		AddVideoTextView.setTypeface(robotoLight);
		AddVideoFromGalleryTextView.setTypeface(robotoLight);
		
		OnClickListener l1loadImgfromCam = new OnClickListener() {
			public void onClick(View v) {
				if (!isDeviceSupportCamera()) {
					Toast.makeText(getActivity(),"Przepraszamy, twoje urzadzenie nie ma aparatu",Toast.LENGTH_LONG).show();
					return;
				}else {
					if(((MainActivity)getActivity()).isGPSEnabled()){
						if(checkStorage()) {
							loadImagefromCamera();
						} else {
							Toast.makeText(getActivity(),"Przed przystąpieniem do korzystania z aparatu włóż kartę SD.",Toast.LENGTH_LONG).show();
						}
					}else{
						((MainActivity)getActivity()).checkGPSsnack();
					}
				}
			}
		};
		
		OnClickListener loadImgfromGall = new OnClickListener() {
			public void onClick(View v) {
				if(((MainActivity)getActivity()).isGPSEnabled()){
					loadImagefromGallery();
					
				}else{
					((MainActivity)getActivity()).checkGPSsnack();
				}
			}
		};
		
		OnClickListener loadImgfromCamFab = new OnClickListener() {
			public void onClick(View view) {
				if(((MainActivity)getActivity()).isGPSEnabled()){
					if (!isDeviceSupportCamera()) {
						Toast.makeText(getActivity(),"Przepraszamy, twoje urzadzenie nie ma aparatu",Toast.LENGTH_LONG).show();
						return;
					} else {
						if(checkStorage()) {
							cameraID = findCamID();
							camera = Camera.open(cameraID);
							isSectedFAB = true;
							onStartCamFAB(view);
						} else {
							Toast.makeText(getActivity(),"Przed przystąpieniem do korzystania z aparatu włóż kartę SD.",Toast.LENGTH_LONG).show();
						}
					}
				}else{
					((MainActivity)getActivity()).checkGPSsnack();
				}
			}
		};
		
		OnClickListener loadVidfromCam = new OnClickListener() {
			public void onClick(View v) {
				if(((MainActivity)getActivity()).isGPSEnabled()){
					loadVideofromCamrea();
					
				}else{
					((MainActivity)getActivity()).checkGPSsnack();
				}
			}
		};
		
		OnClickListener loadVidfromGall = new OnClickListener() {
			public void onClick(View v) {
				if(((MainActivity)getActivity()).isGPSEnabled()){
					loadVideofromGallery();
					
				}else{
					((MainActivity)getActivity()).checkGPSsnack();
				}
			}
		};
		
		buttonAddPhoto.setOnClickListener(l1loadImgfromCam);
		buttonAddQuickPhoto.setOnClickListener(loadImgfromCamFab);
		buttonaddFromGallery.setOnClickListener(loadImgfromGall);
		buttonaddVideoFromGallery.setOnClickListener(loadVidfromGall);
		buttonAddVideo.setOnClickListener(loadVidfromCam);
		
		return v;
	}

	private void releaseCameraAndPreview() {
		camera.stopPreview();
		if (camera != null) {
			//camera.stopPreview();
			camera.release();
			camera = null;
		}
	}
	
	private int findCamID() {
		int camid = -1;
		for(int i = 0; i < Camera.getNumberOfCameras(); ++i) {
			CameraInfo info = new CameraInfo();
			Camera.getCameraInfo(i, info);
			if(info.facing == CameraInfo.CAMERA_FACING_BACK) {
				camid = i;
				break;
			}
		}
		return camid;
	}
	
	public void onStartCamFAB(View view) {
		parameters = camera.getParameters();
		parameters.set("orientation", "portrait");
		parameters.setRotation(90);
		parameters.setPictureFormat(ImageFormat.JPEG);
		parameters.setPictureFormat(PixelFormat.JPEG);
		camera.setParameters(parameters);
		try {
			camera.startPreview();
			camera.takePicture(null, null, this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void onPictureTaken(byte[] data, Camera cam) {
		ACTIONID = 3;
		if(data != null) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(data , 0, data.length);
			if(bitmap != null) {
				File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),AppConfig.IMAGE_DIRECTORY_NAME);
				
				if(!mediaStorageDir.isDirectory()) {
					mediaStorageDir.mkdir();
				}
				
				String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",Locale.getDefault()).format(new Date());
				File mediaFile = new File(mediaStorageDir.getPath() + File.separator+"IMG_"+ timeStamp + ".jpg");
				
				try {
					FileOutputStream fileOutputStream = new FileOutputStream(mediaFile);
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
					fileOutputStream.flush();
					fileOutputStream.close();
				} catch(IOException e) {
					e.printStackTrace();
				} catch(Exception exception) {
					exception.printStackTrace();
				}
				IMAGEVIDEOpath = mediaFile.getAbsolutePath();
			}
		}
		releaseCameraAndPreview();
		launchUploadActivity(true, IMAGEVIDEOpath);
	}
	
	private boolean isDeviceSupportCamera() {
		if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void loadImagefromGallery() {
		ACTIONID = 2;
		Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(galleryIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}
	
	public void loadVideofromGallery() {
		Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
		galleryIntent.setType("video/*");
		startActivityForResult(galleryIntent, CAMERA_REQUEST_CODE_VIDEO);
	}
	
	public void loadImagefromCamera() {
		if(isSectedFAB){camera.stopPreview(); camera = null; isSectedFAB = false;}
		ACTIONID = 1;
		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		fileUri = getOutputMediaFileUri();
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		startActivityForResult(cameraIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}
	
	public void loadVideofromCamrea() {
		Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		if(takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
			startActivityForResult(takeVideoIntent,CAMERA_REQUEST_CODE_VIDEO);
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable("file_uri", fileUri);
	}
	
	public static boolean checkStorage() {
		String state = Environment.getExternalStorageState();
		if (state.equals(Environment.MEDIA_MOUNTED)) {
			return true;
		} else {
			return false;
		}
	}
	
	public String getPath(Uri uri) {
		String[] filePathColumn = {MediaStore.Images.Media.DATA};
		Cursor cursor = getActivity().getContentResolver().query(uri, filePathColumn, null, null, null);
		cursor.moveToFirst();
		int columnIndex=cursor.getColumnIndex(filePathColumn[0]);
		return cursor.getString(columnIndex);
	}
	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent idata) {
		super.onActivityResult(requestCode, resultCode, idata);
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				if(ACTIONID == 2) { 
					selectedImage = idata.getData(); 
					IMAGEVIDEOpath = getPath(selectedImage);
				}
				else if(ACTIONID == 1) { 
					IMAGEVIDEOpath = fileUri.getPath();
				}
				launchUploadActivity(true,IMAGEVIDEOpath);
			} else if(resultCode == Activity.RESULT_CANCELED) {
				Toast.makeText(getActivity(),"Anulowano robienie zdjecia!", Toast.LENGTH_SHORT).show();
				fileUri = null;
				selectedImage = null;
				IMAGEVIDEOpath=null;
				ACTIONID = 0;
			} else {
				Toast.makeText(getActivity(),"Przepraszamy, nie udalo sie zrobic zdjecia", Toast.LENGTH_SHORT).show();
			}
		}
		if(requestCode == CAMERA_REQUEST_CODE_VIDEO) {
			if (resultCode == Activity.RESULT_OK) {
				Uri videoUri = idata.getData();
				IMAGEVIDEOpath = getPath(videoUri);
				launchUploadActivity(false,IMAGEVIDEOpath);
			}
			else if(resultCode == Activity.RESULT_CANCELED) {
				IMAGEVIDEOpath = null;
				Toast.makeText(getActivity(),"Anulowano nagrywanie wideo!", Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(getActivity(),"Przepraszamy, nie udalo sie nagrać wideo", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private void launchUploadActivity(boolean isImage, String path){
		Bundle bundle = new Bundle();
		FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
		bundle.putString("filePath", path);
		bundle.putBoolean("isImage", isImage);
		MediaViewer fragobj = new MediaViewer();
		fragobj.setArguments(bundle);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.replace(R.id.frame,fragobj);
		fragmentTransaction.commit();
	}
	
	public Uri getOutputMediaFileUri() {
		return Uri.fromFile(getOutputMediaFile());
	}
	
	private static File getOutputMediaFile() {
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),AppConfig.IMAGE_DIRECTORY_NAME);
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				return null;
			}
		}
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",Locale.getDefault()).format(new Date());
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator+"IMG_"+ timeStamp + ".jpg");
		return mediaFile;
	}
}