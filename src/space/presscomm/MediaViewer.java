/*
Łukasz Fałda
Numer indeksu: 115973
Wydział: WIMiI
Kierunek: Infromatyka, 1 stopnia
Specialność: Inżynieria oprogramowania
*/
package space.presscomm;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import space.presscomm.AppConfig;

@SuppressWarnings("deprecation")
public class MediaViewer extends Fragment {
	ImageView imgView1 = null, imgViewL = null, imgViewR = null, IMGrec;
	VideoView vidView1 = null;
	private MediaController mediaControls;
	private int position = 0;
	
	LinearLayout addNext, chooseSource, addAud;
	private static String filePath = null, filePathL = null, filePathR = null;
	private static boolean isImage = false;
	private static Uri camIMGuri = null;
	private static String camURI_L = "", camURI_R = "";
	
	private final int GALLERY_IMAGE_REQUEST_CODE = 100;
	private final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 101;
	private final static int MEDIA_TYPE_IMAGE = 1;
	private static int NumPhoto = 0;
	
	private static String comText = null;
	private static boolean isDescript = false;
	
	View buttonaddAnother, buttonaddComment, buttonaddAudio, buttonsendImage, buttoneditAddress,
		buttonFromGallery, buttonFromCamera;
	TextView textaddAnother_text, textaddComment_text, textaddaudio_text,
		textsendImage_text, editAddress_text, fromGallery_text, fromCamera_text;
	public TextView txtMess;
	
	public static TextView AdresTxtView;
	
	Typeface robotoLight, robotoRegular;
	
	ScrollView ScrollText;
	Bitmap bitmap = null;
	
	private ProgressDialog progressBar;
	
	long totalSize = 0;
	public static double LA = 0.0,LO = 0.0;
	public static int USERxID = 0;
	
	private String cDate, cTime;
	
	private MediaRecorder PCARecorder;
	private static  String outputRecFile = null;
	
	private static boolean isRecording = false;
	public static String ADRESS = "", KODP = "";
	
	private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 555;
	
	@SuppressLint("SimpleDateFormat")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_image_viewer,container,false);
		
		((MainActivity)getActivity()).findGPS(true);
		NumPhoto = 0;
		bitmap = null;
		
		robotoLight = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RobotoLight.ttf");  
		robotoRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RobotoRegular.ttf"); 

		filePath = getArguments().getString("filePath");
		isImage = getArguments().getBoolean("isImage");
		
		Toast.makeText(getActivity(), "sciezka: "+filePath, Toast.LENGTH_LONG).show();
		
		imgViewL = (ImageView) v.findViewById(R.id.imageView0);
		imgView1 = (ImageView) v.findViewById(R.id.imageView1);
		imgViewR = (ImageView) v.findViewById(R.id.imageView2);
		
		vidView1 = (VideoView) v.findViewById(R.id.videoView1);
		
		addNext = (LinearLayout) v.findViewById(R.id.addAnotherLay);
		chooseSource = (LinearLayout) v.findViewById(R.id.chooseSourceLay);
		addAud = (LinearLayout) v.findViewById(R.id.addaudio);
		
		txtMess = (TextView) v.findViewById(R.id.mess);
		ScrollText = (ScrollView) v.findViewById(R.id.scrollText);
		AdresTxtView = (TextView) v.findViewById(R.id.mess2);	//****************
		IMGrec = (ImageView) v.findViewById(R.id.imgrec);
		
		buttonaddAnother = v.findViewById(R.id.addAnother);
		buttonaddComment = v.findViewById(R.id.addComment);
		buttonaddAudio = v.findViewById(R.id.addaudio);
		buttonsendImage = v.findViewById(R.id.sendImage);
		buttoneditAddress = v.findViewById(R.id.ManualAdressButton);
		buttonFromGallery = v.findViewById(R.id.FromGalleryLay);
		buttonFromCamera = v.findViewById(R.id.FromCameraLay);
		
		textaddAnother_text = (TextView) v.findViewById(R.id.addAnother_text);
		textaddComment_text = (TextView) v.findViewById(R.id.addComment_text);
		textaddaudio_text = (TextView) v.findViewById(R.id.addaudio_text);
		textsendImage_text = (TextView) v.findViewById(R.id.sendImage_text);
		editAddress_text = (TextView) v.findViewById(R.id.manualadressedittxt);
		fromGallery_text = (TextView) v.findViewById(R.id.FromGalleryTxt);
		fromCamera_text = (TextView) v.findViewById(R.id.FromCameraTxt);
		
		textaddAnother_text.setTypeface(robotoLight);
		textaddComment_text.setTypeface(robotoLight);
		textaddaudio_text.setTypeface(robotoLight);
		textsendImage_text.setTypeface(robotoLight);
		fromGallery_text.setTypeface(robotoLight);
		fromCamera_text.setTypeface(robotoLight);
		txtMess.setTypeface(robotoRegular);
		AdresTxtView.setTypeface(robotoRegular);	
		editAddress_text.setTypeface(robotoRegular);	
		
		if(!isImage) { 
			buttonaddAnother.setVisibility(View.GONE);
			buttonaddAudio.setVisibility(View.GONE);
			addNext.setVisibility(View.GONE);
			addAud.setVisibility(View.GONE);
		}
		
		cDate = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
		cTime = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
		
		OnClickListener addnext = new OnClickListener() {
			public void onClick(View v) { 
				if(NumPhoto != 2) {
					addNext.setVisibility(View.GONE);
					chooseSource.setVisibility(View.VISIBLE);
				}
				else {
					buttonaddAnother.setActivated(false);
					buttonaddAnother.setEnabled(false); 
					textaddAnother_text.setPaintFlags(textaddAnother_text.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
				}
			}
		}; buttonaddAnother.setOnClickListener(addnext);
		
		OnClickListener addfromgallery = new OnClickListener() {
			public void onClick(View v) { 
				addfromGallery();
			}
		}; buttonFromGallery.setOnClickListener(addfromgallery);
		
		OnClickListener addfromgcamera = new OnClickListener() {
			public void onClick(View v) { 
				addfromCamera();
			}
		}; buttonFromCamera.setOnClickListener(addfromgcamera);
		
		//ADD COMMENT
		OnClickListener popupText = new OnClickListener() {
			public void onClick(View v) { editPopup(); }
		}; buttonaddComment.setOnClickListener(popupText);
		
		//SEND FILE
		OnClickListener l = new OnClickListener() {
			public void onClick(View v) { 
				if(LA != 0.0 && LO != 0.0) {
					new UploadFileToServer().execute();
				} else {
					Toast.makeText(getActivity(), "Poczekaj aż zostanie wykryta lokalizacja", Toast.LENGTH_SHORT).show();
				}
			}
		}; buttonsendImage.setOnClickListener(l);
		
		//EDIT ADDRESS
		OnClickListener editADR = new OnClickListener() {
			public void onClick(View v) { 
				showSearchAdresWindow();
			}
		}; buttoneditAddress.setOnClickListener(editADR);
		
		//ADD AUDIO 
		OnClickListener addAudio = new OnClickListener() {
			public void onClick(View v) { recordAudio(); }
		}; buttonaddAudio.setOnClickListener(addAudio);
		
		if(filePath != null) {
			previewMedia(isImage,filePath, 1);
		} else {
			Toast.makeText(getActivity(),"Ścieżka do pliku jest pusta!", Toast.LENGTH_LONG).show();
		}
		
		return v;
	}
	
	//******************************************************************************************
	
	public void addfromGallery() {
		if (Build.VERSION.SDK_INT <= 19) {
			Intent galleryIntent = new Intent();
			galleryIntent.setType("image/*");
			galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
			galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
			startActivityForResult(galleryIntent, GALLERY_IMAGE_REQUEST_CODE);
		} 
		else if(Build.VERSION.SDK_INT > 19) {
			Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(galleryIntent, GALLERY_IMAGE_REQUEST_CODE);
		}
	}
	
	public void addfromCamera() {
		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		camIMGuri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, camIMGuri);
		startActivityForResult(cameraIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}
	
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}
	
	private static File getOutputMediaFile(int type) {
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),AppConfig.IMAGE_DIRECTORY_NAME);
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				return null;
			}
		}
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",Locale.getDefault()).format(new Date());
		File mediaFile;
		if(type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator+"IMG_"+ timeStamp + ".jpg");
		}
		else {
			return null;
		}
		return mediaFile;
	}
	
	public void showSearchAdresWindow() {
		try {
			AutocompleteFilter adressType = new AutocompleteFilter.Builder()
				.setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();
			Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
				.setFilter(adressType).build(getActivity());			
			startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
		} catch (GooglePlayServicesRepairableException e) {
			// TODO: Handle the error.
		} catch (GooglePlayServicesNotAvailableException e) {
			// TODO: Handle the error.
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
			if(resultCode == Activity.RESULT_OK) {
				Place place = PlaceAutocomplete.getPlace(getActivity(), data);
				ADRESS = place.getAddress().toString();
				LA = place.getLatLng().latitude;
				LO = place.getLatLng().longitude;
				
				AdresTxtView.setText(ADRESS);
			} else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
				// TODO
			} else if(resultCode == Activity.RESULT_CANCELED) {
				// TODO
			}
		}
		else if(requestCode == GALLERY_IMAGE_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				if(NumPhoto == 0) {
					filePathL = getPath(data.getData());
					imgViewL.setVisibility(View.VISIBLE);
					addNext.setVisibility(View.VISIBLE);
					chooseSource.setVisibility(View.GONE);
					previewMedia(true,filePathL,0);
				}
				else if(NumPhoto == 1) {
					filePathR = getPath(data.getData()); 
					imgViewR.setVisibility(View.VISIBLE);
					addNext.setVisibility(View.VISIBLE);
					chooseSource.setVisibility(View.GONE);
					previewMedia(true,filePathR,2);
				}
				NumPhoto++;
			} else if(resultCode == Activity.RESULT_CANCELED) {
				Toast.makeText(getActivity(),"Anulowano wczytywanie zdjecia", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getActivity(),"Przepraszamy, nie udalo sie wczytac zdjecia", Toast.LENGTH_SHORT).show();
			}
		}
		else if(requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				if(NumPhoto == 0) {
					filePathL = camIMGuri.getPath();
					imgViewL.setVisibility(View.VISIBLE);
					addNext.setVisibility(View.VISIBLE);
					chooseSource.setVisibility(View.GONE);
					previewMedia(true,filePathL,0);
				}
				else if(NumPhoto == 1) {
					filePathR = camIMGuri.getPath();
					imgViewR.setVisibility(View.VISIBLE);
					addNext.setVisibility(View.VISIBLE);
					chooseSource.setVisibility(View.GONE);
					previewMedia(true,filePathR,2);
				}
				NumPhoto++;
			} else if(resultCode == Activity.RESULT_CANCELED) {
				Toast.makeText(getActivity(),"Anulowano wczytywanie zdjecia", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getActivity(),"Przepraszamy, nie udalo sie wczytac zdjecia", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	public String getPath(Uri uri) {
		String[] filePathColumn = {MediaStore.Images.Media.DATA};
		Cursor cursor = getActivity().getContentResolver().query(uri, filePathColumn, null, null, null);
		cursor.moveToFirst();
		int columnIndex=cursor.getColumnIndex(filePathColumn[0]);
		return cursor.getString(columnIndex);
	}

	public void recordAudio() {
		if(isRecording == false) {
			File CheckDirectory;
			CheckDirectory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/PressCommAudio");
			
			if(!CheckDirectory.exists()) { 
				CheckDirectory.mkdir();
			}
			
			outputRecFile = CheckDirectory + "/recording.mp3";
			PCARecorder=new MediaRecorder();
			PCARecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			PCARecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			PCARecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
			PCARecorder.setAudioEncodingBitRate(16);
			PCARecorder.setAudioSamplingRate(44100);
			PCARecorder.setOutputFile(outputRecFile);
		
			try {
				PCARecorder.prepare();
				PCARecorder.start();
			}
			catch(IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			textaddaudio_text.setText("zatrzymaj");
			IMGrec.setImageResource(R.drawable.ic_rec_r);
			Toast.makeText(getActivity(), "Rozpoczęto nagrywanie", Toast.LENGTH_SHORT).show();
			isRecording = true;
		}
		else {
			PCARecorder.stop();
			PCARecorder.release();
			PCARecorder  = null;
			Toast.makeText(getActivity(), "Nagrano pomyślnie\nścieżka zapisu: "+outputRecFile,Toast.LENGTH_SHORT).show();
			IMGrec.setImageResource(R.drawable.ic_rec);
			textaddaudio_text.setText("od nowa");
			isRecording = false;
		}
	}

	public void editPopup() {
		LayoutInflater li = LayoutInflater.from(getActivity());
		View promptsView = li.inflate(R.layout.popupedittext, null);
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
		alertDialogBuilder.setView(promptsView);
		
		final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);
		if(isDescript) {userInput.setText(comText);}
		
		alertDialogBuilder.setCancelable(false)
			.setPositiveButton("Potwierdź",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					comText = userInput.getText().toString();
					
					if(TextUtils.isEmpty(comText)) {
						Toast.makeText(getActivity(), "nie!", Toast.LENGTH_SHORT).show();
					}
					else {
						ScrollText.setVisibility(View.VISIBLE);
						txtMess.setVisibility(View.VISIBLE);
						txtMess.setText(comText);
						textaddComment_text.setText(R.string.editcomment);
						isDescript = true;
					}
				}
			})
			.setNegativeButton("Anuluj",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
				}
			});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}


	public void previewMedia(boolean isImg, String path, int nrimgViev) {
		if (isImg) {
			imgView1.setVisibility(View.VISIBLE);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path, options);
			double imageHeight = options.outHeight/3;
			double imageWidth = options.outWidth/3;
			if(nrimgViev == 0) {
				imgViewL.setImageBitmap(decodeSampledBitmapFromFile(path, imageWidth, imageHeight));
			}
			if(nrimgViev == 1) {
				imgView1.setImageBitmap(decodeSampledBitmapFromFile(path, imageWidth, imageHeight));
			}
			if(nrimgViev == 2) {
				imgViewR.setImageBitmap(decodeSampledBitmapFromFile(path, imageWidth, imageHeight));
			}
		}
		else {
			vidView1.setVisibility(View.VISIBLE);
			try {
				mediaControls = new MediaController(getActivity());
				mediaControls.setAnchorView(vidView1);
				vidView1.setMediaController(mediaControls);
				vidView1.setVideoPath(path);
			} catch(Exception e) {
				e.printStackTrace();
			}
			vidView1.requestFocus();
			
			vidView1.setOnPreparedListener(new OnPreparedListener() {
				public void onPrepared(MediaPlayer mediaPlayer) {
					mediaPlayer.seekTo(position);
					if(position == 0) {
						mediaPlayer.start();
					}
					else {
						mediaPlayer.pause();
					}
				}
			});
		}
	}
	
	public static int calculateInSampleSize(BitmapFactory.Options options, double reqWidth, double reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		
		if(height > reqHeight || width > reqWidth) {
			final int halfHeight = height / 2;
			final int halfWidth = width / 2;
			
			while((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}
		return inSampleSize;
	}
	
	public static Bitmap decodeSampledBitmapFromFile(String file, double reqWidth, double reqHeight) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(file, options);
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(file, options);
	}
	
	private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
		@Override
		protected void onPreExecute() {
			progressBar=new ProgressDialog(getActivity(),ProgressDialog.THEME_HOLO_LIGHT);
			progressBar.setMessage("Wysyłanie...");
			progressBar.setCancelable(false);
			progressBar.setProgressNumberFormat(null);
			progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progressBar.setIndeterminate(false);
			progressBar.setProgress(0);
			progressBar.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {
			return uploadFile();
		}
		
		@SuppressWarnings({ "resource" })
		private String uploadFile() {
			String responseString = null;
			
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(AppConfig.FILE_UPLOAD_URL);
			 
			try {
				MultipartEntity pack = new MultipartEntity();
				File sourceFile = new File(filePath);
				File sourceFileAudio;
				
				File sourceFileL, sourceFileR;
				
				if(outputRecFile != null) { 
					sourceFileAudio = new File(outputRecFile);
					pack.addPart("recordaudio", new FileBody(sourceFileAudio)); 
				}
				
				if(NumPhoto == 1) {
					sourceFileL = new File(filePathL);
					pack.addPart("imageL", new FileBody(sourceFileL));
				}
				else if(NumPhoto == 2) {
					sourceFileL = new File(filePathL);
					sourceFileR = new File(filePathR);
					pack.addPart("imageL", new FileBody(sourceFileL));
					pack.addPart("imageR", new FileBody(sourceFileR));
				}
				
				if(isImage) {
					pack.addPart("image", new FileBody(sourceFile));
				}
				else {
					pack.addPart("video", new FileBody(sourceFile));
				}
				Charset chars = Charset.forName("UTF-8");
				USERxID = MainActivity.USERxID;
				
				pack.addPart("xid",new StringBody(""+USERxID));
				pack.addPart("long",new StringBody(""+LO));
				pack.addPart("lati", new StringBody(""+LA));
				pack.addPart("mess", new StringBody(""+comText,chars));
				pack.addPart("cdate", new StringBody(""+cDate));
				pack.addPart("ctime", new StringBody(""+cTime));
				pack.addPart("adress", new StringBody(""+ADRESS,chars));

				httppost.setEntity(pack);

				HttpResponse response = httpclient.execute(httppost);
				HttpEntity r_pack = response.getEntity();
				
				int statusCode = response.getStatusLine().getStatusCode();
				if(statusCode == 200) {
					responseString = EntityUtils.toString(r_pack);
				}else {
					responseString = "Wystąpił błąd! Kod : "+ statusCode;
				}
			} catch (ClientProtocolException e) {
				responseString = e.toString();
			} catch (IOException e) {
				responseString = e.toString();
			} finally {
				httpclient.getConnectionManager().shutdown();
			}
			return responseString;
		}
	
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressBar.hide();
			progressBar.dismiss();
			showAlert(result);
		}
	}
	private void showAlert(String a) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage("ok")
		.setMessage(a)
		.setCancelable(false)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public void onResume() {
		super.onResume();
		if(/*bitmap == null && */filePath != null) {
			previewMedia(isImage,filePath, 1);
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//((MainActivity)getActivity()).IMGvAdd = false;
		MainActivity.IMGvAdd = false;
		imgView1.setImageDrawable(null);
	}
}