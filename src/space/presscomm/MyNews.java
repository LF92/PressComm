/*
Łukasz Fałda
Numer indeksu: 115973
Wydział: WIMiI
Kierunek: Infromatyka, 1 stopnia
Specialność: Inżynieria oprogramowania
*/
package space.presscomm;

import space.presscomm.NewsElement;
import space.presscomm.ListforNews;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

public class MyNews extends Fragment {
	private static String url = "";
	private ProgressDialog pDialog;
	private List<NewsElement> newsLIST = new ArrayList<NewsElement>();
	private ListView listView;
	private ListforNews adapter;
	public static int ID = 0;
	private PopupWindow pwindo;
	private MediaPlayer PlayRecAudio = null;
	ImageButton playpause_popup;
	
	private ProgressDialog dialog;
	RotateAnimation anim;
	ImageView IM;
	
	private TextView toolbarText;
	
	RequestQueue queue;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_my_news,container,false);
		ID = MainActivity.USERxID;
		queue = Volley.newRequestQueue(getActivity());
		toolbarText = (TextView) v.findViewById(R.id.txtTOOLBAR);
		toolbarText.setText("Moje");
		
		anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(Animation.INFINITE);
		anim.setDuration(700);
		
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("http").authority("anddev.one").appendPath("n.php").appendQueryParameter("id", ""+ID);
		url = builder.build().toString();
		
		listView = (ListView) v.findViewById(R.id.list);
		adapter = new ListforNews(getActivity(), newsLIST);
		
		listView.setAdapter(adapter);
		newsLIST.clear();
		loadNews();

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
				NewsElement entry= (NewsElement) parent.getAdapter().getItem(position);
				String imgurl = entry.getImg1();
				String adress = entry.getAdress();
				String datee = entry.getDatee();
				String timee = entry.getTimee();
				String datetime = datee+" "+timee;
				String recaudio = entry.getRecaud();
				String messagee = entry.getMess();
				
				initiatePopupWindow(imgurl,adress,datetime,recaudio,messagee);
			}
		});
		return v;
	}
	
	@SuppressWarnings("deprecation")
	private void initiatePopupWindow(String IMGURL, String ADRESS, String DATETIME, String RECA, String MESS) {
		try {
			LayoutInflater inflat = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflat.inflate(R.layout.details_item,(ViewGroup) getActivity().findViewById(R.id.popup_element));
			
			IM = (ImageView) layout.findViewById(R.id.imgPOPUP);
			Button close = (Button) layout.findViewById(R.id.btn_close_popup);
			TextView adress_popup = (TextView) layout.findViewById(R.id.adressPOPUP);
			TextView datetime_popup = (TextView) layout.findViewById(R.id.datetimePOPUP);
			TextView messagee_popup = (TextView) layout.findViewById(R.id.txtViewPOPUP);
			playpause_popup = (ImageButton) layout.findViewById(R.id.playpausePOPUP);
			
			pwindo = new PopupWindow(layout, LayoutParams.MATCH_PARENT, LayoutParams.FILL_PARENT, true);
			pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
			
			new DownImg(IM).execute(IMGURL);
			
			final String aaa = RECA;
			
			adress_popup.setText(ADRESS);
			datetime_popup.setText(DATETIME);
			
			if(MESS != null && !MESS.isEmpty()) {
				messagee_popup.setVisibility(View.VISIBLE);
				messagee_popup.setText(MESS);
			}
		
			if(RECA != null && !RECA.isEmpty()) {
				playpause_popup.setVisibility(View.VISIBLE);
				
				OnClickListener l = new OnClickListener() {
					public void onClick(View v) { 
						clickPlay(aaa); }
				}; playpause_popup.setOnClickListener(l);
			}
		
			close.setOnClickListener(new OnClickListener() { 
				public void onClick(View popupView) {
					pwindo.dismiss();
				}
			});
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clickPlay(String a) {
		if(PlayRecAudio == null) {
			new PlayM().execute(a);
		}
		else {
			stopPlayREC();
		}
	}
	
	public class PlayM extends AsyncTask<String,Void,Void> {
		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(getActivity());
			dialog.setMessage("czekaj...");
			dialog.setIndeterminate(true);
			dialog.show();
			PlayRecAudio = new MediaPlayer();
			PlayRecAudio.setAudioStreamType(AudioManager.STREAM_MUSIC);
			playpause_popup.setImageResource(R.drawable.ic_stat_av_pause);
		}
		
		@Override
		protected Void doInBackground(String... params) {
			try {
				PlayRecAudio.setDataSource(params[0]);
				PlayRecAudio.prepareAsync();
			} catch (IOException e3) {
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			dialog.dismiss();
			dialog.hide();
			PlayRecAudio.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer player) {
					player.start();
					player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
						@Override
						public void onCompletion(MediaPlayer mp) {
							//mp.reset();
							mp.release();
							mp = null;
							stopPlayREC();
							playpause_popup.setImageResource(R.drawable.ic_stat_av_play_arrow);
						}
					});
				}
			});
		}
	}
	
	public void stopPlayREC() {
		if(PlayRecAudio != null) {
			PlayRecAudio.release();
			PlayRecAudio = null;
			playpause_popup.setImageResource(R.drawable.ic_stat_av_play_arrow);
		}
	}
	
	private class DownImg extends AsyncTask<String, Void, Bitmap> {
		ImageView tymIMG;
		
		public DownImg(ImageView tmpIMG) {
			this.tymIMG = tmpIMG;
		}
		
		@Override
		protected void onPreExecute() {
			IM.startAnimation(anim);
		}
		
		protected Bitmap doInBackground(String... urls) {
			String URL = urls[0];
			Bitmap mIMG = null;
		
			try {
				InputStream in = new java.net.URL(URL).openStream();
				mIMG = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return mIMG;
		}
		
		protected void onPostExecute(Bitmap result) {
			IM.setAnimation(null);
			tymIMG.setImageBitmap(result);
		}
	}
	
	@SuppressWarnings("deprecation")
	public void loadNews() {
		pDialog = new ProgressDialog(getActivity(),ProgressDialog.THEME_HOLO_LIGHT);
		pDialog.setMessage("Wczytywanie...");
		pDialog.show();
		
		JsonArrayRequest newsReq = new JsonArrayRequest(url,new Response.Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				hidePDialog();
				for (int i = 0; i < response.length(); i++) {
					try {
						JSONObject obj = response.getJSONObject(i);
						NewsElement news = new NewsElement();
						news.setAdress(obj.getString("N_ADRE"));
						news.setImg1(obj.getString("N_IMG1"));
						news.setMess(obj.getString("N_MESS"));
						news.setDatee(obj.getString("N_DATE"));
						news.setTimee(obj.getString("N_TIME"));
						news.setNewsID(obj.getInt("N_NID"));
						news.setRecaud(obj.getString("N_RECA"));
						
						newsLIST.add(news);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				adapter.notifyDataSetChanged();
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				hidePDialog();
			}
		});
		queue.add(newsReq);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		newsLIST.clear();
		hidePDialog();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		newsLIST.clear();
	}

	private void hidePDialog() {
		if (pDialog != null) {
			pDialog.dismiss();
			pDialog = null;
		}
	}
}