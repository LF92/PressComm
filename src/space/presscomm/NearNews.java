/*
Łukasz Fałda
Numer indeksu: 115973
Wydział: WIMiI
Kierunek: Infromatyka, 1 stopnia
Specialność: Inżynieria oprogramowania
*/
package space.presscomm;

import space.presscomm.NewsElement;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NearNews extends Fragment {
	private MapView mMapView;
	private GoogleMap googleMap;
	
	private List <LatLng> points = new ArrayList<LatLng>();
	private List <MarkerOptions> markers = new ArrayList<MarkerOptions>();
	private List <NewsElement> allnewsLIST = new ArrayList<NewsElement>();

	private ListforNews alladapter;
	private ProgressDialog pdDialog;
	
	private static String url = "";
	
	RequestQueue queue;

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.near_news, container, false);
		queue = Volley.newRequestQueue(getActivity());
		pdDialog = new ProgressDialog(getActivity(),ProgressDialog.THEME_HOLO_LIGHT);
		pdDialog.setMessage("Wczytywanie...");
		
		double latitude = MainActivity.LATI;
		double longitude = MainActivity.LONG;
		url = AppConfig.URL_ALLNEWS;
		
		alladapter = new ListforNews(getActivity(), allnewsLIST);
		loadNews();
		
		mMapView = (MapView) v.findViewById(R.id.map);
		mMapView.onCreate(savedInstanceState);
		mMapView.onResume();

		googleMap = mMapView.getMap();
		
		CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(12).build();
		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		googleMap.setMyLocationEnabled(true);
		
		return v;
	}
	
	public void loadNews() {
		pdDialog.show();
		
		JsonArrayRequest newsReq = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				hidePDialog();
				for(int i = 0; i < response.length(); i++) {
					try {
						JSONObject obj = response.getJSONObject(i);
						NewsElement news = new NewsElement();
						
						news.setAdress(obj.getString("N_ADRE"));
						news.setImg1(obj.getString("N_IMG1"));
						news.setMess(obj.getString("N_MESS"));
						news.setDatee(obj.getString("N_DATE"));
						news.setTimee(obj.getString("N_TIME"));
						news.setLatitude(obj.getString("N_LATI"));
						news.setLongitude(obj.getString("N_LONG"));
						
						allnewsLIST.add(news);
						
						NewsElement entry= (NewsElement) alladapter.getItem(i);
						
						final String MESSGMS = entry.getMess();
						final String ADRESV = entry.getAdress();
						double xLA = Double.parseDouble(entry.getLatitude());
						double xLO = Double.parseDouble(entry.getLongitude());
						
						points.add(new LatLng(xLA,xLO));
							
						MarkerOptions marker = new MarkerOptions().position(points.get(i)).title("Wydarzenie #"+i).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)).snippet(ADRESV).snippet(ADRESV+"\n\n"+MESSGMS);;
							
						googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
							@Override public View getInfoWindow(Marker arg0){return null;}
							
							@Override
							public View getInfoContents(Marker marker) {
								LinearLayout info = new LinearLayout(getActivity());
								info.setOrientation(LinearLayout.VERTICAL);
								TextView title = new TextView(getActivity());
								title.setTextColor(Color.BLACK);
								title.setGravity(Gravity.CENTER);
								title.setTypeface(null, Typeface.BOLD);
								title.setText(marker.getTitle());
								TextView snippet = new TextView(getActivity());
								snippet.setTextColor(Color.GRAY);
								snippet.setText(marker.getSnippet());
								info.addView(title);
								info.addView(snippet);
								return info;
							}
						});
						markers.add(marker);
						googleMap.addMarker(markers.get(i));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				alladapter.notifyDataSetChanged();
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				hidePDialog();
				}
			});
			queue.add(newsReq);
		}
	
	private void hidePDialog() {
		if (pdDialog != null) {
			pdDialog.dismiss();
			pdDialog = null;
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mMapView.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		mMapView.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mMapView.onDestroy();
	}
}