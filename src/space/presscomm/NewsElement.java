/*
Łukasz Fałda
Numer indeksu: 115973
Wydział: WIMiI
Kierunek: Infromatyka, 1 stopnia
Specialność: Inżynieria oprogramowania
*/
package space.presscomm;

public class NewsElement {
	private String LATI="", LONGI="", ADRESS="", MESS="", DATEE="", TIMEE="",
					IMG1="", IMG2="", IMG3="", RECAUD="", EMAIL="", UNAME="";
	private int NEWS_ID=0, USER_ID=0;
	
	public NewsElement() {}

	public NewsElement(
			int news_id,
			int user_id, 
			String lati,
			String longi,
			String adress,
			String mess,
			String datee,
			String timee,
			String img1,
			String img2,
			String img3,
			String recaud,
			String email,
			String uname
			) {
		this.NEWS_ID = news_id;
		this.USER_ID = user_id;
		this.LATI = lati;
		this.LONGI = longi;
		this.ADRESS = adress;
		this.MESS = mess;
		this.DATEE = datee;
		this.TIMEE = timee;
		this.IMG1 = img1;
		this.IMG2 = img2;
		this.IMG3 = img3;
		this.RECAUD = recaud;
		this.EMAIL = email;
		this.UNAME = uname;
	}

	public int getNewsID() {return NEWS_ID; }
	public void setNewsID(int news_id) {this.NEWS_ID = news_id; }
	
	public int getUserID() {return USER_ID; }
	public void setUserID(int user_id) {this.USER_ID = user_id; }
	
	public String getLatitude() {return LATI;}
	public void setLatitude(String lati) {this.LATI = lati;}
	
	public String getLongitude() {return LONGI;}
	public void setLongitude(String longi) {this.LONGI = longi;}
	
	public String getAdress() {return ADRESS;}
	public void setAdress(String adress) {this.ADRESS = adress;}
	
	public String getMess() {return MESS;}
	public void setMess(String mess) {this.MESS = mess;}
	
	public String getDatee() {return DATEE;}
	public void setDatee(String datee) {this.DATEE = datee;}
	
	public String getTimee() {return TIMEE;}
	public void setTimee(String timee) {this.TIMEE = timee;}
	
	public String getImg1() {return IMG1;}
	public void setImg1(String img1) {this.IMG1 = img1;}
	
	public String getImg2() {return IMG2;}
	public void setImg2(String img2) {this.IMG2 = img2;}
	
	public String getImg3() {return IMG3;}
	public void setImg3(String img3) {this.IMG3 = img3;}
	
	public String getRecaud() {return RECAUD;}
	public void setRecaud(String recaud) {this.RECAUD = recaud;}
	
	public String getEmail() {return EMAIL;}
	public void setEmail(String email) {this.EMAIL = email;}
	
	public String getUname() {return UNAME;}
	public void setUname(String uname) {this.UNAME = uname;}

}
