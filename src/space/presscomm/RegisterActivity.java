/*
Łukasz Fałda
Numer indeksu: 115973
Wydział: WIMiI
Kierunek: Infromatyka, 1 stopnia
Specialność: Inżynieria oprogramowania
*/
package space.presscomm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import space.presscomm.AppConfig;
import space.presscomm.localDB;

public class RegisterActivity extends Activity {
	final Context context = RegisterActivity.this;
	private Button buttonREGISTER;
	private EditText editTXTFullName, editTXTEmail, editTXTPassword;
	private ProgressDialog pDialog;
	private localDB DB;
	RequestQueue queue;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		queue = Volley.newRequestQueue(this);
		
		editTXTFullName = (EditText) findViewById(R.id.name);
		editTXTEmail = (EditText) findViewById(R.id.email);
		editTXTPassword = (EditText) findViewById(R.id.password);
		buttonREGISTER = (Button) findViewById(R.id.btnRegister);
		
		pDialog = new ProgressDialog(context);
		pDialog.setCancelable(false);

		DB = new localDB(context);

		buttonREGISTER.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				String name = editTXTFullName.getText().toString().trim();
				String email = editTXTEmail.getText().toString().trim();
				String password = editTXTPassword.getText().toString().trim();
				
				if(!name.isEmpty() && !email.isEmpty() && !password.isEmpty()) {
					registerUser(name, email, password);
				}
				else {
					Toast.makeText(context,R.string.allMustBeFilled, Toast.LENGTH_LONG).show();
				}
			}
		});
	}
	
	private void closeOtherActivity() {
		MainActivity.MAIN_ACTIVITY.finish();
		LoginActivity.LOGIN_ACTIVITY.finish();
	}

	private void registerUser(final String name, final String email, final String password) {
		pDialog.setMessage(getString(R.string.waitRegister));
		showDialog();
		
		StringRequest strReq = new StringRequest(Method.POST,AppConfig.URL_REGISTER, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				hideDialog();
				
				try {
					JSONObject jObj = new JSONObject(response);
					boolean error = jObj.getBoolean("error");
					
					if(!error) {
						String xid = jObj.getString("xid");
						String uid = jObj.getString("uid");
						JSONObject user = jObj.getJSONObject("user");
						String name = user.getString("name");
						String email = user.getString("email");
						String created_at = user.getString("created_at");
						
						DB.addUser(name, email, xid, uid, created_at);
						Toast.makeText(context,R.string.register_ok, Toast.LENGTH_LONG).show();
						closeOtherActivity();
						Intent intent = new Intent(context,LoginActivity.class);
						startActivity(intent);
						finish();
					}
					else {
						String errorMsg = jObj.getString("error_msg");
						Toast.makeText(context, "1: "+errorMsg, Toast.LENGTH_LONG).show();
					}
				}
				catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Toast.makeText(getApplicationContext(),"getmess: >>"+error.getMessage()+"<<, Err: >>"+error+"<<", Toast.LENGTH_LONG).show();
				hideDialog();
			}
		}) {
	
			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("name", name);
				params.put("email", email);
				params.put("password", password);
				return params;
			}

		};
		queue.add(strReq);
	}
	
	private void showDialog() { if (!pDialog.isShowing()) { pDialog.show();  } }
	private void hideDialog() { if (pDialog.isShowing()) { pDialog.dismiss(); } }
}