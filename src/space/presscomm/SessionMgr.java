/*
Łukasz Fałda
Numer indeksu: 115973
Wydział: WIMiI
Kierunek: Infromatyka, 1 stopnia
Specialność: Inżynieria oprogramowania
*/
package space.presscomm;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionMgr {
	SharedPreferences PREF;
	Editor EDITOR;
	Context context;
	int PRIVATE_MODE = 0;
	
	private static final String PREF_NAME = "PressComm";
	private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
	
	@SuppressLint("CommitPrefEdits")
	public SessionMgr(Context _context) {
		this.context = _context;
		PREF = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		EDITOR = PREF.edit();
	}
	
	public void setLogin(boolean isLoggedIn) {
		EDITOR.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
		EDITOR.commit();
	}
	
	public boolean isLoggedIn(){
		return PREF.getBoolean(KEY_IS_LOGGEDIN, false);
	}
}