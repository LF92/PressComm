/*
Łukasz Fałda
Numer indeksu: 115973
Wydział: WIMiI
Kierunek: Infromatyka, 1 stopnia
Specialność: Inżynieria oprogramowania
*/
package space.presscomm;

import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class localDB extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	
	private static final String DATABASE_NAME = "presscomDB";
	private static final String TABLE_USER = "user";
	
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_EMAIL = "email";
	private static final String KEY_XID = "xid";
	private static final String KEY_UID = "uid";
	private static final String KEY_CREATED_AT = "created_at";
	
	public localDB(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
		+ KEY_EMAIL + " TEXT UNIQUE," + KEY_XID + " TEXT,"+ KEY_UID + " TEXT," + KEY_CREATED_AT + " TEXT" + ")";
		db.execSQL(CREATE_LOGIN_TABLE);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		onCreate(db);
	}
	
	public void addUser(String name, String email, String xid, String uid, String created_at) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, name);
		values.put(KEY_EMAIL, email);
		values.put(KEY_XID, xid);
		values.put(KEY_UID, uid);
		values.put(KEY_CREATED_AT, created_at);
		
		@SuppressWarnings("unused")
		long id = db.insert(TABLE_USER, null, values);
		db.close();
	}
	
	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		String selectQuery = "SELECT  * FROM " + TABLE_USER;
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		cursor.moveToFirst();
		if(cursor.getCount() > 0) {
			user.put("name", cursor.getString(1));
			user.put("email", cursor.getString(2));
			user.put("xid", cursor.getString(3));
			user.put("uid", cursor.getString(4));
			user.put("created_at", cursor.getString(5));
		}
		cursor.close();
		db.close();
		return user;
	}
		
	public void deleteUsers() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_USER, null, null);
		db.close();
	}
}